package com.cgi.log.analyser.file.service;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.cgi.log.analyser.data.model.LogEntry;
import com.cgi.log.analyser.parser.LogParserService;

@AutoConfigureMockMvc
@SpringBootTest
@RunWith(SpringRunner.class)
public class FileReaderTest {
	
	@Test
	public void testFileProcessor_Pass() throws Exception {
		FileReader fileReader = new FileReader(null);
		Path currentDir = Paths.get("");
		String absolutePath = currentDir.toAbsolutePath()+"\\src\\test\\resources\\logFile-2018-09-10.log";
		
		//fileReader.fileProcessor(absolutePath);
	}

}
