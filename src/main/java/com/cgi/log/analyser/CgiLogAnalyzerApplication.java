package com.cgi.log.analyser;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import com.cgi.log.analyser.config.StorageProperties;
import com.cgi.log.analyser.file.service.StorageService;

@SpringBootApplication
@EnableConfigurationProperties(StorageProperties.class)
public class CgiLogAnalyzerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CgiLogAnalyzerApplication.class, args);
	}

	@Bean
	CommandLineRunner init(StorageService storageService) {
		return args -> {
			storageService.deleteAll();
			storageService.init();
		};
	}
}
