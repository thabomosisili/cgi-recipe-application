package com.cgi.log.analyser.file.exception;

public class StorageException  extends RuntimeException{
	public StorageException(String message) {
		super(message);
	}

	public StorageException(String message, Throwable cause) {
		super(message, cause);
	}

}
