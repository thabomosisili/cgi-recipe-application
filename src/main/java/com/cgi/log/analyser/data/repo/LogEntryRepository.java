package com.cgi.log.analyser.data.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cgi.log.analyser.data.model.LogEntry;

@Repository 
public interface LogEntryRepository  extends JpaRepository<LogEntry, Long> { 
	Optional<LogEntry> findLogEntryById(Long id);
	
	
	  @Query(value =
	  "SELECT c.date, c.type, c.description, COUNT(*) as occurances, " +
	  "FROM LogEntry AS c WHERE c.type = :logType  GROUP BY c.description ORDER BY occurances DESC", nativeQuery = true) 
	  List<?>sortByLogTypeFrequency(@Param("logType") String logType);
	 

	
	 @Query("SELECT c FROM LogEntry c where c.type = :logType") 
	 List<LogEntry> findByLogType(@Param("logType") String logType);
	 

}
