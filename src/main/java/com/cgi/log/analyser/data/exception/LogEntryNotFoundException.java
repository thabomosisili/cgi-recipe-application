package com.cgi.log.analyser.data.exception;

public class LogEntryNotFoundException extends RuntimeException {
	
	public LogEntryNotFoundException(String message){
		super(message);	
	}
	 

}
